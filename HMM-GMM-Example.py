
# coding: utf-8

# In[65]:


# http://hmmlearn.readthedocs.io/en/latest/auto_examples/index.html
# http://www.blackarbs.com/blog/introduction-hidden-markov-models-python-networkx-sklearn/2/9/2017

import pandas as pd
import pandas_datareader.data as web
import sklearn.mixture as mix

import numpy as np
import scipy.stats as scs

import matplotlib as mpl
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.dates import YearLocator, MonthLocator
get_ipython().run_line_magic('matplotlib', 'inline')

import seaborn as sns
import missingno as msno
from tqdm import tqdm
p=print


# # import data

# In[51]:


data = pd.read_csv("/home/alexey/Downloads/data_stocks.csv")
data[['NYSE.YUM','NYSE.ZBH','NYSE.ZTS']].head()


# # slice data

# In[52]:


X= data[['NYSE.YUM','NYSE.ZBH','NYSE.ZTS']].dropna()


# # train GMM(initialization) and train markov states(predict)

# In[53]:


model = mix.GaussianMixture(n_components=3, 
                            covariance_type="full", 
                            n_init=100, 
                            random_state=8).fit(X)

# Predict the optimal sequence of internal hidden state
hidden_states = model.predict(X)


# # sampling

# In[54]:


model.sample(1)


# # scoring

# In[63]:


model.score_samples([[76,110,60],[73,110,60],[746,110,60]])

